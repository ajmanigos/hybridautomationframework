package dataprovider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
    private Properties properties;
    private final String propertyFilePath = "configs/configuration.properties";

    public ConfigFileReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public String getChromeDriverPath(){
        String driverPath = properties.getProperty("ChromeDriverPath");
        if(driverPath!= null) return driverPath;
        else throw new RuntimeException("chromeDriverPath not specified in the Configuration.properties file.");
    }

    public String getFirefoxDriverPath() {
        String driverPath = properties.getProperty("FirefoxDriverPath");
        if(driverPath != null) return driverPath;
        else throw new RuntimeException("firefoxDriverPath not specified in the Configuration.properties file.");
    }

    public String getInternetExplorerDriverPath() {
        String driverPath = properties.getProperty("InternetExplorerDriverPath");
        if(driverPath != null) return driverPath;
        else throw new RuntimeException("internetExplorerDriverPath not specified in the Configuration.properties file.");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("ImplicitWait");
        if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
        else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");
    }

    public String getApplicationUrl() {
        String url = properties.getProperty("ApplicationURL");
        if(url != null) return url;
        else throw new RuntimeException("internetExplorerDriverPath not specified in the Configuration.properties file.");
    }

}
