package pagetest;

import core.DriverManager;
import core.DriverManagerFactory;
import core.DriverType;
import managers.FileReaderManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageobject.GoogleSearchHomePage;

import java.util.concurrent.TimeUnit;

public class GoogleSearchHomePageTest {

    GoogleSearchHomePage googleSearchHomePage;
    DriverManager driverManager;
    WebDriver driver;

    @BeforeClass
    public void setup(){
         driverManager = DriverManagerFactory.getDriverManagerType(DriverType.CHROME);
         driver = driverManager.getWebDriver();
         driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
         driver.manage().window().maximize();
         driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
         googleSearchHomePage = new GoogleSearchHomePage(driver);
    }

    @Test
    public void searchKeyword1(){
        googleSearchHomePage.enterSearchKeyword("Cheese");
        googleSearchHomePage.clickSearchButton();
    }

    @AfterClass
    public void tearDown(){
        driverManager.quitWebDriver();
    }
}
