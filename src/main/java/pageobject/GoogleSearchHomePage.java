package pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearchHomePage {
    public GoogleSearchHomePage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    @FindBy(name = "q")private WebElement searchBarField;
    @FindBy(name = "btnK")private WebElement searchButton;

    public void enterSearchKeyword(String valueToEnter){
        searchBarField.sendKeys(valueToEnter);
    }

    public void clickSearchButton(){
        searchButton.click();
    }
}
