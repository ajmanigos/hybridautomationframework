package core.chrome;

import core.DriverManager;
import managers.FileReaderManager;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverManager extends DriverManager {
    @Override
    public void createWebDriver(){
         System.setProperty("webdriver.chrome.driver", FileReaderManager.getInstance().getConfigReader().getChromeDriverPath());
         this.driver = new ChromeDriver();
    }
}
