package core.safari;

import core.DriverManager;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

public class SafariDriverManager extends DriverManager {
    @Override
    public void createWebDriver(){
        SafariOptions safariOptions = new SafariOptions();
        this.driver = new SafariDriver(safariOptions);
    }
}
