package core.ie;

import core.DriverManager;
import managers.FileReaderManager;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class InternetExplorerDriverManager extends DriverManager {
    @Override
    public void createWebDriver(){
        System.setProperty("webdriver.ie.driver", FileReaderManager.getInstance().getConfigReader().getInternetExplorerDriverPath());
        this.driver = new InternetExplorerDriver();
    }
}
