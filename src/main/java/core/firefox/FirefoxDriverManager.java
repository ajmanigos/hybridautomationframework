package core.firefox;

import core.DriverManager;
import managers.FileReaderManager;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManager {
    @Override
    public void createWebDriver(){
        System.setProperty("webdriver.firefox.driver", FileReaderManager.getInstance().getConfigReader().getFirefoxDriverPath());
        this.driver = new FirefoxDriver();
    }
}
