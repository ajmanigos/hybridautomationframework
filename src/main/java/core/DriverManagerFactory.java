package core;

import core.chrome.ChromeDriverManager;
import core.firefox.FirefoxDriverManager;
import core.ie.InternetExplorerDriverManager;
import core.safari.SafariDriverManager;

public class DriverManagerFactory {
    public static DriverManager getDriverManagerType(DriverType driver){
        DriverManager driverManager;

        switch (driver){
            case CHROME:
                driverManager = new ChromeDriverManager();
                break;
            case FIREFOX:
                driverManager = new FirefoxDriverManager();
                break;
            case IE:
                driverManager = new InternetExplorerDriverManager();
                break;
            default:
                driverManager = new SafariDriverManager();
          }
        return driverManager;
    }
}
